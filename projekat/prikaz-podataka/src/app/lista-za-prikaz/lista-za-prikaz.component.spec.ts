import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaZaPrikazComponent } from './lista-za-prikaz.component';

describe('ListaZaPrikazComponent', () => {
  let component: ListaZaPrikazComponent;
  let fixture: ComponentFixture<ListaZaPrikazComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaZaPrikazComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaZaPrikazComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
