import { Component, OnInit } from '@angular/core';
import { PrikazPodataka } from 'src/app/model/prikaz-podataka'
import { Router, ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-lista-za-prikaz',
  templateUrl: './lista-za-prikaz.component.html',
  styleUrls: ['./lista-za-prikaz.component.css']
})
export class ListaZaPrikazComponent implements OnInit {
  public listaPodataka:Array<any> = [{'ime':'Mina', 'prezime':'Gusman', 'datumRodjenja':'30.12.1989.', 'zanimanje':'student', 'bojaKose':'braon', 'bojaOciju':'braon'},
                                    {'ime':'Maja', 'prezime':'Nikolic', 'datumRodjenja':'30.09.1982.', 'zanimanje':'Customer care', 'bojaKose':'braon', 'bojaOciju':'braon'}, 
                                    {'ime':'Avram', 'prezime':'Nikolic', 'datumRodjenja':'30.04.2016', 'zanimanje':'nema', 'bojaKose':'plava', 'bojaOciju':'zelena'},];
  public prikazZaListu:Array<String> = ['ime','prezime'];
  public naslov:String = 'Lista za prikaz';
  // public podaciZaListu:Array<any> = null;
  public jsonUbacivanje:any = [];
  public vidljivost:boolean = false;
  public element:any = null;
  public kljucevi:Array<string> = [];
  public prevodKljuceva:Array<string> = [];

  constructor(private httpClient:HttpClient, private router: Router, private aRoute:ActivatedRoute) { }

  ngOnInit(): void {
    

  }

  prikazTabele(element): void{
    this.vidljivost = true;
    this.element = element;
    // this.kljucevi = Object.keys(element)

    this.kljucevi = Object.keys(element);
    this.prevodKljuceva = this.kljucevi.map(kljuc=>
      kljuc.match(/[A-Z]?[a-z]+/g).join(" "))
    console.log(element)
  }

}
