import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListaZaPrikazComponent} from './lista-za-prikaz/lista-za-prikaz.component'


const routes: Routes = [
  {path: 'prikaz', component: ListaZaPrikazComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
