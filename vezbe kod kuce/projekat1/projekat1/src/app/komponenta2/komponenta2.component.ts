import { Component, OnInit, OnDestroy } from "@angular/core";
import { MagicniBrojServis } from "../servis-magicnibroj.service";
import { Observer } from "../model/observer";
import { Observable } from "../model/observable";

@Component({
  selector: "app-komponenta2",
  templateUrl: "./komponenta2.component.html",
  styleUrls: ["./komponenta2.component.css"],
})
export class Komponenta2Component implements OnInit, OnDestroy, Observer {
  public broj: number;
  public noviBroj: number;
  constructor(private brojServis: MagicniBrojServis) {}

  onClick = (inputEl: HTMLInputElement): void => {
    // console.log(+inputEl.value);
    this.brojServis.setBroj(+inputEl.value);
  };

  update(): void {
    this.broj = this.brojServis.getBroj();
  }

  ngOnInit(): void {
    this.broj = this.brojServis.getBroj();
    this.brojServis.attach(this);
  }
  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this.brojServis.detach(this);
  }
}
