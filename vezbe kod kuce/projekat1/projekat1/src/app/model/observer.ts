import { Observable } from "./observable";

export declare interface Observer {
  update(): void;
}
