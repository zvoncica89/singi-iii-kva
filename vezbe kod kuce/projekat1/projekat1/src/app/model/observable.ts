import { Observer } from "./observer";

export interface Observable {
  attach(observer: Observer): boolean;
  detach(observer: Observer): boolean;
  notify(): void;
}
