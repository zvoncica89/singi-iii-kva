import { Injectable } from "@angular/core";
import { Observable } from "./model/observable";
import { Observer } from "./model/observer";

@Injectable({ providedIn: "root" })
export class MagicniBrojServis implements Observable {
  private broj: number = 1;
  private listaObservera: Set<Observer> = new Set();
  attach(observer: Observer): boolean {
    if (this.listaObservera.has(observer)) {
      return false;
    }
    this.listaObservera.add(observer);
    return true;
  }
  detach(observer: Observer): boolean {
    if (this.listaObservera.has(observer)) {
      this.listaObservera.delete(observer);
      return true;
    }
    return false;
  }

  notify(): void {
    for (let observer of this.listaObservera) {
      observer.update();
    }
  }

  getBroj() {
    return this.broj;
  }

  setBroj(noviBroj: number) {
    this.broj = noviBroj;
    this.notify();
  }
}
