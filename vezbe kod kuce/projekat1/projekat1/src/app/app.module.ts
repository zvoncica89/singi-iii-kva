import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { Komponenta1Component } from './komponenta1/komponenta1.component';
import { Komponenta2Component } from './komponenta2/komponenta2.component';

@NgModule({
  declarations: [
    AppComponent,
    Komponenta1Component,
    Komponenta2Component
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
