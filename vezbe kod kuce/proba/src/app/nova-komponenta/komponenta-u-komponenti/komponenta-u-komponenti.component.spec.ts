import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KomponentaUKomponentiComponent } from './komponenta-u-komponenti.component';

describe('KomponentaUKomponentiComponent', () => {
  let component: KomponentaUKomponentiComponent;
  let fixture: ComponentFixture<KomponentaUKomponentiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KomponentaUKomponentiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KomponentaUKomponentiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
