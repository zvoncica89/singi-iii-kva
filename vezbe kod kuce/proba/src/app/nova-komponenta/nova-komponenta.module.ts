import { NgModule } from "@angular/core";
import { NovaKomponentaComponent } from './nova-komponenta.component';
import { KomponentaUKomponentiComponent } from './komponenta-u-komponenti/komponenta-u-komponenti.component';
import { CommonModule } from '@angular/common';

@NgModule({
    declarations: [NovaKomponentaComponent, KomponentaUKomponentiComponent],
    imports: [CommonModule],
    exports:[NovaKomponentaComponent, KomponentaUKomponentiComponent]
})
export class NovaKomponentaModule {}