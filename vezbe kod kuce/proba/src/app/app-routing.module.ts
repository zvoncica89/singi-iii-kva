import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NovaKomponentaComponent } from './nova-komponenta/nova-komponenta.component';
import { KomponentaUKomponentiComponent } from './nova-komponenta/komponenta-u-komponenti/komponenta-u-komponenti.component';


const routes: Routes = [
  {
    path: 'novaKomponenta', component: NovaKomponentaComponent },
  {
    path: 'novaKomponenta/kuk', component: KomponentaUKomponentiComponent
  }, {
    path: '**', redirectTo: 'novaKomponenta'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
