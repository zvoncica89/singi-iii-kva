import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {NovaKomponentaModule} from './nova-komponenta/nova-komponenta.module'

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NovaKomponentaModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
