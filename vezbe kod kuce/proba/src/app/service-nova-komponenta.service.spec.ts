import { TestBed } from '@angular/core/testing';

import { ServiceNovaKomponentaService } from './service-nova-komponenta.service';

describe('ServiceNovaKomponentaService', () => {
  let service: ServiceNovaKomponentaService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ServiceNovaKomponentaService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
