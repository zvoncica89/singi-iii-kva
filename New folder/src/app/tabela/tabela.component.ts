import { Component, OnInit, Input, OnChanges, SimpleChange, SimpleChanges, Output, EventEmitter } from '@angular/core';
import { CRUDService } from '../crud.service';

@Component({
  selector: 'app-tabela',
  templateUrl: './tabela.component.html',
  styleUrls: ['./tabela.component.css'],
})
export class TabelaComponent implements OnInit, OnChanges {
  @Input()
  public podaci: JSON[] = [];
  public kolone: string[] = [];
  public prikazi = false;
  @Output() naObrisi: EventEmitter<any> = new EventEmitter();
  @Output() naIzmeni: EventEmitter<any> = new EventEmitter();
  @Output() naDetalji: EventEmitter<any> = new EventEmitter();

  public filmovi: any;
  constructor(private crudServis: CRUDService) {}

  ngOnInit(): void {
    // console.log(this.podaci);
    

  }

  ngOnChanges(changes: SimpleChanges) {
    console.log(changes);
    if(changes.podaci != undefined && changes.podaci.currentValue != [] && changes.podaci.currentValue != undefined) {
      this.kolone = Object.keys(this.podaci[0]);
      this.prikazi = true;
    }
  }

  obrisi(podatak: any) {
    // console.log(podatak);
    this.naObrisi.emit(podatak);
  }
  izmeni(podatak: any) {
    // console.log(podatak);
    this.naIzmeni.emit(podatak);
  }
  detalji(podatak: any) {
    // console.log(podatak);
    this.naDetalji.emit(podatak);
  }


}
