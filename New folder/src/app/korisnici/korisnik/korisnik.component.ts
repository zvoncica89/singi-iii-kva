import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { Korisnik } from 'src/app/model/korisnik';
import { CRUDService } from 'src/app/crud.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-korisnik',
  templateUrl: './korisnik.component.html',
  styleUrls: ['./korisnik.component.css']
})
export class KorisnikComponent implements OnInit {
  public korisnik: Korisnik = {
    id:null,
    korisnickoIme:"",
    email:"",
    lozinka:""
  }
  public izmena: boolean = false;
  constructor(private httpClient:HttpClient, private router: Router, private aRoute: ActivatedRoute, private crudService: CRUDService) { }


  ngOnInit(): void {
    this.izmena = (this.aRoute.snapshot.queryParams.izmeni =="true");
    this.crudService.getOne("http://localhost:3000/korisnik/" + this.aRoute.snapshot.params.id).subscribe((korisnik:Korisnik)=>{
      this.korisnik = korisnik;
    }, err => {console.log(err)})
  }

  submit(form:NgForm){
    console.log(this.korisnik)
   this.crudService.updateOne("http://localhost:3000/korisnik/" + this.korisnik.id, this.korisnik).subscribe((korisnik:Korisnik)=>{
     window.alert("Uspesno izmenjen korisnik!");
     this.router.navigate(["/korisnici"]);
   }, err =>(console.log(err)))
  }

  nazad(){
    this.router.navigate(["/korisnici"]);
  }

}
