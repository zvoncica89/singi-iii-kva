import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { CRUDService } from '../crud.service';
import { Korisnik } from '../model/korisnik';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-korisnici',
  templateUrl: './korisnici.component.html',
  styleUrls: ['./korisnici.component.css']
})
export class KorisniciComponent implements OnInit {
  public podaci : Korisnik[] = [];
  public dodaj: boolean = false;
  public korisnik: Korisnik = {
    id:null,
    korisnickoIme:"",
    email:"",
    lozinka:""
  }
  constructor(private httpClient:HttpClient, private router: Router, private cdrudService: CRUDService) { }

  getAllKorisnici(){
    this.cdrudService.getAll("http://localhost:3000/korisnik").subscribe((povratnaVrednost:any[])=>{
      
      this.podaci = povratnaVrednost;
      console.log(this.podaci);
      }, err=> {console.log(err)});
  }

  ngOnInit(): void {
    this.getAllKorisnici();
  }

  naObrisi(korisnik: Korisnik): void {
    this.cdrudService.deleteOne("http://localhost:3000/korisnik/" + korisnik.id).subscribe(result => {
      this.podaci = this.podaci.filter(f => f != korisnik);
    })
    console.log(korisnik);
  }

  naIzmeni(korisnik: Korisnik): void {
    this.router.navigate(['korisnici', korisnik.id], {queryParams: {izmeni : true}});
    console.log(korisnik);
  }
  naDetalji(korisnik: Korisnik): void {
    this.router.navigate(['korisnici', korisnik.id], {queryParams: {izmeni : false}});
  }

  submit(form: NgForm){
    console.log(this.korisnik);
    this.cdrudService.createOne("http://localhost:3000/korisnik", this.korisnik).subscribe((korisnik:Korisnik)=>{
        window.alert("Uspesno dodat novi korisnik");
        this.getAllKorisnici();
        this.dodaj = false;
        
      }, err=> {console.log(err)})
    
  }

}
