import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FilmoviComponent } from './filmovi/filmovi.component';
import { FilmComponent } from './filmovi/film/film.component';
import { KorisniciComponent } from './korisnici/korisnici.component';
import { KorisnikComponent } from './korisnici/korisnik/korisnik.component';
import { OceneComponent } from './ocene/ocene.component';
import { OcenaComponent } from './ocene/ocena/ocena.component';

const routes: Routes = [
    {path: 'filmovi', component: FilmoviComponent},
    {path: 'filmovi/:id', component: FilmComponent},
    {path: 'korisnici', component: KorisniciComponent},
    {path: 'korisnici/:id', component: KorisnikComponent},
    {path: 'ocene', component: OceneComponent},
    {path: 'ocene/:id', component: OcenaComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }