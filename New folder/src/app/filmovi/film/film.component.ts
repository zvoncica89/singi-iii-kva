import { Component, OnInit } from '@angular/core';
import { Film } from 'src/app/model/film';
import { HttpClient } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { CRUDService } from 'src/app/crud.service';
import { NgForm } from '@angular/forms';
import { Ocena } from 'src/app/model/ocena';

@Component({
  selector: 'app-film',
  templateUrl: './film.component.html',
  styleUrls: ['./film.component.css']
})
export class FilmComponent implements OnInit {
  public film : Film = {
    id:null,
    naziv:"",
    reziser:"",
    ocena: null
  }
  public ocene = [];
  public izmena: boolean = false;
  constructor(private httpClient:HttpClient, private router: Router, private aRoute: ActivatedRoute, private crudService: CRUDService) { }

  getAllOcene(){
    this.crudService.getAll("http://localhost:3000/ocena").subscribe((povratnaVrednost:any[])=>{

    for (const ocena of povratnaVrednost) {
      if( ocena.id == this.aRoute.snapshot.params.id){
        this.ocene.push(ocena);
      }
      console.log(this.ocene);
    }
      }, err=> {console.log(err)});
  }
  ngOnInit(): void {
    this.izmena = (this.aRoute.snapshot.queryParams.izmeni =="true");
    this.crudService.getOne("http://localhost:3000/film/" + this.aRoute.snapshot.params.id).subscribe((film:Film)=>{
      this.film = film;
      this.getAllOcene();
    }, err => {console.log(err)})
  }

  submit(form:NgForm){
    console.log(this.film)
   this.crudService.updateOne("http://localhost:3000/film/" + this.film.id, this.film).subscribe((film:Film)=>{
     window.alert("Uspesno izmenjen film!");
     this.router.navigate(["/filmovi"]);
   }, err =>(console.log(err)))
  }

  nazad(){
    this.router.navigate(["/filmovi"]);
  }

}
