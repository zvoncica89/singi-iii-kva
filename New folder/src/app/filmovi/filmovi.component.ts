import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { CRUDService } from '../crud.service';
import { Film } from '../model/film';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-filmovi',
  templateUrl: './filmovi.component.html',
  styleUrls: ['./filmovi.component.css']
})
export class FilmoviComponent implements OnInit {
  public podaci : Film[] = [];
  public dodaj: boolean = false;
  public film : Film = {
    id:null,
    naziv:"",
    reziser:"",
    ocena: null
  }
  constructor(private httpClient:HttpClient, private router: Router, private cdrudService: CRUDService) { }

  getAllFilmovi(){
    this.cdrudService.getAll("http://localhost:3000/film").subscribe((povratnaVrednost:any[])=>{
      
      this.podaci = povratnaVrednost;
      console.log(this.podaci);
      }, err=> {console.log(err)});
  }
  ngOnInit(): void {
    this.getAllFilmovi();
  }

  naObrisi(film: Film): void {
    this.cdrudService.deleteOne("http://localhost:3000/film/" + film.id).subscribe(result => {
      this.podaci = this.podaci.filter(f => f != film);
    })
    console.log(film);
  }

  naIzmeni(film: Film): void {
    this.router.navigate(['filmovi', film.id], {queryParams: {izmeni : true}});
    console.log(film);
  }
  naDetalji(film: Film): void {
    this.router.navigate(['filmovi', film.id], {queryParams: {izmeni : false}});
  }
  

  submit(form: NgForm){
    console.log(this.film);
    this.cdrudService.createOne("http://localhost:3000/film", this.film).subscribe((film:Film)=>{
        window.alert("Uspesno dodat novi film");
        this.getAllFilmovi();
        this.dodaj = false;
        
      }, err=> {console.log(err)})
    
  }

}
