import { Component, OnInit } from '@angular/core';
import { Ocena } from '../model/ocena';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { CRUDService } from '../crud.service';
import { Film } from '../model/film';
import { Korisnik } from '../model/korisnik';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-ocene',
  templateUrl: './ocene.component.html',
  styleUrls: ['./ocene.component.css']
})
export class OceneComponent implements OnInit {
  public podaci : Ocena[] = [];
  public filmovi : Film[] = [];
  public korisnici : Korisnik[] = [];
  public dodaj: boolean = false;
  public novaOcena : Ocena = {
    id: null,
    filmId: null,
    korisnikId: null,
    ocena: null,
    komentar: ""
  }
  constructor(private httpClient:HttpClient, private router: Router, private cdrudService: CRUDService) { }

  getAllOcene(){
    this.cdrudService.getAll("http://localhost:3000/ocena").subscribe((povratnaVrednost:any[])=>{
      
      this.podaci = povratnaVrednost;
      console.log(this.podaci);
      }, err=> {console.log(err)});
  }
  getAllFilmovi(){
    this.cdrudService.getAll("http://localhost:3000/film").subscribe((povratnaVrednost:any[])=>{
      
      this.filmovi = povratnaVrednost;

      }, err=> {console.log(err)});
  }
  getAllKorisnici(){
    this.cdrudService.getAll("http://localhost:3000/korisnik").subscribe((povratnaVrednost:any[])=>{
      
      this.korisnici = povratnaVrednost;

      }, err=> {console.log(err)});
  }

  ngOnInit(): void {
    this.getAllOcene();
    this.getAllFilmovi();
    this.getAllKorisnici();
  }

  naObrisi(ocena: Ocena): void {
    this.cdrudService.deleteOne("http://localhost:3000/ocena/" + ocena.id).subscribe(result => {
      this.podaci = this.podaci.filter(f => f != ocena);
    })
    console.log(ocena);
  }

  naIzmeni(ocena: Ocena): void {
    this.router.navigate(['ocene', ocena.id], {queryParams: {izmeni : true}});
    console.log(ocena);
  }
  naDetalji(ocena: Ocena): void {
    this.router.navigate(['ocene', ocena.id], {queryParams: {izmeni : false}});
  }

  submit(form: NgForm){
    console.log(this.novaOcena);
    this.cdrudService.createOne("http://localhost:3000/ocena", this.novaOcena).subscribe((ocena:Ocena)=>{
        window.alert("Uspesno dodata nova ocena");
        this.getAllOcene();
        this.dodaj = false;
        
      }, err=> {console.log(err)})
    
  }

}
