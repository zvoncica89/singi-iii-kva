import { Component, OnInit } from '@angular/core';
import { Ocena } from 'src/app/model/ocena';
import { HttpClient } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { CRUDService } from 'src/app/crud.service';
import { Film } from 'src/app/model/film';
import { Korisnik } from 'src/app/model/korisnik';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-ocena',
  templateUrl: './ocena.component.html',
  styleUrls: ['./ocena.component.css']
})
export class OcenaComponent implements OnInit {
  public ocenaPrikaz : Ocena = {
    id: null,
    filmId: null,
    korisnikId: null,
    ocena: null,
    komentar: ""
  }
  public filmovi: Film[] = [];
  public korisnici: Korisnik[] = [];
  public izmena: boolean = false;
  constructor(private httpClient:HttpClient, private router: Router, private aRoute: ActivatedRoute, private crudService: CRUDService) { }

  getAllFilmovi() {
    this.crudService.getAll("http://localhost:3000/film").subscribe((povratnaVrednost:any[])=>{
      
      this.filmovi = povratnaVrednost;
      console.log(this.filmovi);
      }, err=> {console.log(err)});
    
  }

  getAllKorisnici() {
    this.crudService.getAll("http://localhost:3000/korisnik").subscribe((povratnaVrednost:any[])=>{
      
      this.korisnici = povratnaVrednost;
      console.log(this.filmovi);
      }, err=> {console.log(err)});
    
  }
  getAllOcene(){
    this.izmena = (this.aRoute.snapshot.queryParams.izmeni =="true");
    this.httpClient.get("http://localhost:3000/ocena/"+this.aRoute.snapshot.params.id).subscribe((ocena:Ocena)=>{
        this.ocenaPrikaz = ocena;
      }, err=> {console.log(err)})
  }

  ngOnInit(): void {
    this.getAllFilmovi();
    this.getAllKorisnici();
    this.getAllOcene();
  }

  submit(form:NgForm){
   this.crudService.updateOne("http://localhost:3000/ocena/" + this.ocenaPrikaz.id, this.ocenaPrikaz).subscribe((ocena:Ocena)=>{
     window.alert("Uspesno izmenjena ocena!");
     this.router.navigate(["/ocene"]);
   }, err =>(console.log(err)))
  }

  nazad(){
    this.router.navigate(["/ocene"]);
  }

}
