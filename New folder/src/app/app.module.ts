import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TabelaComponent } from './tabela/tabela.component';
import { FilmoviComponent } from './filmovi/filmovi.component';
import { FilmComponent } from './filmovi/film/film.component';
import { KorisniciComponent } from './korisnici/korisnici.component';
import { KorisnikComponent } from './korisnici/korisnik/korisnik.component';
import { OceneComponent } from './ocene/ocene.component';
import { OcenaComponent } from './ocene/ocena/ocena.component';

@NgModule({
  declarations: [
    AppComponent,
    TabelaComponent,
    FilmoviComponent,
    FilmComponent,
    KorisniciComponent,
    KorisnikComponent,
    OceneComponent,
    OcenaComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
