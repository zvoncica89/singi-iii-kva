import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class CRUDService {

  constructor(private httpClient:HttpClient, private router: Router) { }

  getAll(path:string) : Observable<any> {
    return this.httpClient.get(path);
    // this.httpClient.get(path).subscribe((povratnaVrednost:any[])=>{
    //   console.log(povratnaVrednost);
    //     return povratnaVrednost;
    //   }, err=> {console.log(err)})
  }

  getOne(path:string) : Observable<any>{
    return this.httpClient.get(path);
  }

  deleteOne(path:string) : Observable<any>{
    return this.httpClient.delete(path);
  }

  updateOne(path:string, objekat:any):Observable<any>{
    return this.httpClient.put(path, objekat);
    // this.httpClient.put(path, JSON.stringify(objekat)).subscribe((povratnaVrednost:any)=>{
    //   return povratnaVrednost;
    // }, err=> {console.log(err)})
  }

  createOne(path:string, objekat:any) : Observable<any>{
    return this.httpClient.post(path, objekat);
  }

}
