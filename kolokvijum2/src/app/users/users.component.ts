import { Component, OnInit } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { LoginService } from "../login.service";
import { User } from "../model/user";
import { Router } from "@angular/router";

@Component({
  selector: "app-users",
  templateUrl: "./users.component.html",
  styleUrls: ["./users.component.css"],
})
export class UsersComponent implements OnInit {
  public users: any = [];
  constructor(
    private http: HttpClient,
    public loginService: LoginService,
    private router: Router
  ) {}

  getAllUsers() {
    this.http.get("http://localhost:8083/api/users").subscribe((r) => {
      console.log(this.users);
      this.users = r;
    });
  }
  ngOnInit(): void {
    this.getAllUsers();
  }
  brisanje(u: User): void {
    if (u["roles"].includes("ROLE_ADMIN")) {
      console.log("nedozvoljeno");
    } else {
      this.http
        .delete("http://localhost:8083/api/users/" + u.id)
        .subscribe((result) => {
          this.users = this.users.filter((f) => (f = !u));
          this.getAllUsers();
        });
    }
  }

  izmena(u: User): void {
    console.log(u.id);
    this.router.navigate(["/user", u.id]);
  }
  dodavanje() {
    this.router.navigate(["/registracija"]);
  }
  detalji(u: User) {
    this.router.navigate(["/users", u.id]);
  }
}
