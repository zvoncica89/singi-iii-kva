import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { LoginComponent } from "./login/login.component";
import { UsersComponent } from "./users/users.component";
import { RegistracijaComponent } from "./registracija/registracija.component";
import { IzmenaComponent } from "./izmena/izmena.component";
import { DetaljiComponent } from "./detalji/detalji.component";
import { RolesComponent } from "./roles/roles.component";

const routes: Routes = [
  {
    path: "login",
    component: LoginComponent,
  },
  {
    path: "users",
    component: UsersComponent,
  },
  {
    path: "registracija",
    component: RegistracijaComponent,
  },
  {
    path: "user/:id",
    component: IzmenaComponent,
  },
  {
    path: "users/:id",
    component: DetaljiComponent,
  },
  {
    path: "roles",
    component: RolesComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
