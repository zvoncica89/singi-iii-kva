import { Component, OnInit } from "@angular/core";
import { Role } from "../model/Role";
import { HttpClient } from "@angular/common/http";
import { LoginService } from "../login.service";
import { Router } from "@angular/router";

@Component({
  selector: "app-roles",
  templateUrl: "./roles.component.html",
  styleUrls: ["./roles.component.css"],
})
export class RolesComponent implements OnInit {
  public roles: any = [];
  constructor(
    private http: HttpClient,
    public loginService: LoginService,
    private router: Router
  ) {}

  getAllRoles() {
    this.http.get("http://localhost:8083/api/roles").subscribe((r) => {
      console.log(this.roles);
      this.roles = r;
    });
  }

  ngOnInit(): void {}

  brisanje(r: Role): void {
    this.http
      .delete("http://localhost:8083/api/users/" + r.id)
      .subscribe((result) => {
        this.roles = this.roles.filter((f) => (f = !r));
        this.getAllRoles();
      });
  }

  izmena(r: Role): void {
    console.log(r.id);
    this.router.navigate(["/role", r.id]);
  }
  dodavanje() {
    this.router.navigate(["/novaRola"]);
  }
  detalji(r: Role) {
    this.router.navigate(["/roles", r.id]);
  }
}
