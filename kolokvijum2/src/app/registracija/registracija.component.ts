import { Component, OnInit } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { LoginService } from "../login.service";
import { Router } from "@angular/router";
import { NgForm } from "@angular/forms";
import { User } from "../model/user";

@Component({
  selector: "app-registracija",
  templateUrl: "./registracija.component.html",
  styleUrls: ["./registracija.component.css"],
})
export class RegistracijaComponent implements OnInit {
  public noviKorisnik: User = {
    id: null,
    username: "",
    password: "",
    roles: [],
  };
  public roles: any = [];

  constructor(
    private http: HttpClient,
    public loginService: LoginService,
    private router: Router
  ) {}

  getAllRoles() {
    this.http.get("http://localhost:8083/api/roles").subscribe((r) => {
      console.log(this.roles);
      this.roles = r;
    });
  }

  ngOnInit(): void {
    this.getAllRoles();
  }

  submit(form: NgForm) {
    this.http
      .post("http://localhost:8083/api/users", this.noviKorisnik)
      .subscribe(
        (u: User) => {
          window.alert("Uspesno kreiran korisnik");
          this.router.navigate(["/users"]);
        },
        (err) => console.log("Neuspesno")
      );
  }
}
