import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { LoginComponent } from "./login/login.component";
import { UsersComponent } from "./users/users.component";
import { AuthInterceptor } from "./auth.interceptor";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";

import { DragDropModule } from "@angular/cdk/drag-drop";
import { RegistracijaComponent } from "./registracija/registracija.component";
import { IzmenaComponent } from './izmena/izmena.component';
import { DetaljiComponent } from './detalji/detalji.component';
import { RolesComponent } from './roles/roles.component';
// import { BarChartComponent } from "./bar-chart/bar-chart.component";

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    UsersComponent,
    RegistracijaComponent,
    IzmenaComponent,
    DetaljiComponent,
    RolesComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    DragDropModule,
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
