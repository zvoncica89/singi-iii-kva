Kolokvijum 1
Napraviti Angular aplikaciju po sledećim zahtevima:

Podići json-server incijalizovan podacima datim u listingu. Za svaku od dobijenih ruta obezbediti po jednu komponentu za tabelarni prikaz svih podataka na ruti. Iz tabelarnog prikaza omogućiti prikaz detalja izabranog entiteta i njegovo brisanje
Napraviti komponente za prikaz detalja pojedinačnog entiteta, kao i komponente za dodavanje entiteta. Pri kreiranju entiteta koji sadrže veze ka drugim entitetima obezbediti da se izbor povezanih entiteta vrši kroz padajuće liste.
Podesiti rutiranje tako da se za svaki entitet omogući pristup prikazu tabele i forme za dodavanje na jednoj ruti, na drugoj ruti prikaz podataka pojedinačnog entiteta i na trećoj ruti prikaz forme za izmenu podataka entiteta.
U prikazu rаčuna dodati i prikaz svih stavki koje se nalaze na datom računu. Ispod prikaza stavki ispisati ukupnu cenu računa.
Omogućiti filtriranje tabela po vrednostima zadatim za proizvoljne kolone. Demonistrirati ispravnost na barem jednoj tabeli.


{
   "artikal": [
      {
         "id": 1,
         "naziv": "Hleb",
         "cena": 45.5
      },
      {
         "id": 2,
         "naziv": "Mleko",
         "cena": 78.39
      },
      {
         "id": 3,
         "naziv": "So",
         "cena": 40.0
      },
      {
         "id": 4,
         "naziv": "Voda",
         "cena": 25.0
      }
   ],
   "stavkaRacuna": [
      {
         "id": 1,
         "racunId": 1,
         "artikalId": 1,
         "cena": 40.0,
         "kolicina": 1
      },
      {
         "id": 2,
         "racunId": 1,
         "artikalId": 2,
         "cena": 80.5,
         "kolicina": 2
      },
      {
         "id": 3,
         "racunId": 1,
         "artikalId": 3,
         "cena": 45.0,
         "kolicina": 1
      },
      {
         "id": 4,
         "racunId": 3,
         "artikalId": 2,
         "cena": 78.39,
         "kolicina": 1
      }
   ],
   "racun": [
      {
         "id": 1,
         "datumRacuna": "2020-01-06"
      },
      {
         "id": 2,
         "datumRacuna": "2020-02-16"
      },
      {
         "id": 3,
         "datumRacuna": "2020-02-03"
      },
      {
         "id": 4,
         "datumRacuna": "2020-03-26"
      }
   ]
}