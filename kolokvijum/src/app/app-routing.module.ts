import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RacuniComponent } from './racuni/racuni.component';
import { ArtikliComponent } from './artikli/artikli.component';
import { StavkeRacunaComponent } from './stavke-racuna/stavke-racuna.component';
import { RacunComponent } from './racuni/racun/racun.component';
import { ArtikalComponent } from './artikli/artikal/artikal.component';
import { StavkaRacunaComponent } from './stavke-racuna/stavka-racuna/stavka-racuna.component'

const routes: Routes = [
  {path: 'racuni', component: RacuniComponent },
  {path: 'racuni/:id', component: RacunComponent },
  {path: 'artikli', component: ArtikliComponent},
  {path: 'artikli/:id', component: ArtikalComponent},
  {path: 'stavke', component: StavkeRacunaComponent},
  {path: 'stavke/:id', component: StavkaRacunaComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
