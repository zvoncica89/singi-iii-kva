import { Component, OnInit } from '@angular/core';
import { Artikal } from 'src/app/model/artikal';
import { HttpClient } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-artikal',
  templateUrl: './artikal.component.html',
  styleUrls: ['./artikal.component.css']
})
export class ArtikalComponent implements OnInit {
  public artikal: Artikal = {
    id: null,
    naziv: "",
    cena: null
  }

  public izmena: boolean = false;

  constructor(private httpClient:HttpClient, private router: Router, private aRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.izmena = (this.aRoute.snapshot.queryParams.izmeni =="true");
    console.log(this.izmena);
    this.httpClient.get("http://localhost:3000/artikal/"+this.aRoute.snapshot.params.id).subscribe((artikal:Artikal)=>{
        this.artikal = artikal;
      }, err=> {console.log(err)})
  }

  submit(form: NgForm){
    this.httpClient.put("http://localhost:3000/artikal/"+ this.artikal.id, this.artikal).subscribe((artiakl:Artikal)=>{
      window.alert("Uspesno izmenjen artiakl");
      this.router.navigate(["/artikli"]);
    }, err=>{console.log(err)}
    )

  }
  nazad(){
    this.router.navigate(["/artikli"]);
  }

}
