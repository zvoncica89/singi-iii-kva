import { Component, OnInit } from '@angular/core';
import { Artikal } from '../model/artikal';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-artikli',
  templateUrl: './artikli.component.html',
  styleUrls: ['./artikli.component.css']
})
export class ArtikliComponent implements OnInit {
  artikli: Artikal[] = [];
  dodaj: boolean = false;
  noviArtikal: Artikal = {
    id: null,
    naziv: "",
    cena: null
  }

  constructor(private httpClient:HttpClient, private router: Router) { }

  

  ngOnInit(): void {
    this.getAllArtikli()
  }

  getAllArtikli():void{
    this.httpClient.get("http://localhost:3000/artikal").subscribe((artikli:Artikal[])=>{
        this.artikli = artikli;
      }, err=> {console.log(err)})
  }

  ukloni(id){
    this.httpClient.delete("http://localhost:3000/artikal/" + id).subscribe((artikal:Artikal)=>{
      window.alert("Obrisan artikal");
      this.getAllArtikli();
    }, err=>{console.log(err)})
  }

  detalji(id){
    this.router.navigate(['artikli', id], {queryParams: {izmeni : false}});
    
  }

  izmeni(id){
    this.router.navigate(['artikli', id], {queryParams: {izmeni : true}});
  }

  submit(form: NgForm){
    this.httpClient.post("http://localhost:3000/artikal", this.noviArtikal).subscribe((artikal:Artikal)=>{
        window.alert("Uspesno dodat novi artikal");
        this.getAllArtikli();
        this.dodaj = false;
      }, err=> {console.log(err)})
    
  }

}
