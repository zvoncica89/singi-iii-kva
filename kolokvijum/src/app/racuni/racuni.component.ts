import { Component, OnInit } from '@angular/core';
import { Racun } from "../model/racun";
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { NgForm } from '@angular/forms';
import { StavkaRacunaComponent } from '../stavke-racuna/stavka-racuna/stavka-racuna.component';
import { StavkaRacuna } from '../model/stavkaRacuna';

@Component({
  selector: 'app-racuni',
  templateUrl: './racuni.component.html',
  styleUrls: ['./racuni.component.css']
})
export class RacuniComponent implements OnInit {
  racuni: Racun[]=[];
  dodaj: boolean = false;
  prikaz: boolean = false;
  noviRacun: Racun = {
    id: null,
    datumRacuna: null
  }
  sveStavke:StavkaRacuna[]=[];
  stavkeRacuna:StavkaRacuna[] = [];

  constructor(private httpClient:HttpClient, private router: Router) { }

  ngOnInit(): void {
    this.getAllRacuni();
  }

  getAllRacuni():void{
    this.httpClient.get("http://localhost:3000/racun").subscribe((racuni:Racun[])=>{
        this.racuni = racuni;
      }, err=> {console.log(err)})
  }

  ukloni(id){
    this.httpClient.delete("http://localhost:3000/racun/" + id).subscribe((racun:Racun)=>{
      window.alert("Obrisan racun");
      this.getAllRacuni();
    }, err=>{console.log(err)})
  }

  detalji(id){
    this.router.navigate(['racuni', id], {queryParams: {izmeni : false}});
    
  }
  
  izmeni(id){
    this.router.navigate(['racuni', id], {queryParams: {izmeni : true}});
  }
  submit(form: NgForm){
    this.httpClient.post("http://localhost:3000/racun", this.noviRacun).subscribe((racuni:Racun)=>{
        window.alert("Uspesno dodat novi racun");
        this.getAllRacuni();
        this.dodaj = false;
      }, err=> {console.log(err)})
    
  }

  prikaziStavke(id){
    console.log(this.prikaz)
    
    if (this.prikaz == false){
    this.prikaz = true;
    this.httpClient.get("http://localhost:3000/stavkaRacuna").subscribe((stavke:StavkaRacuna[])=>{
        this.sveStavke = stavke;
        for (let i = 0; i<this.sveStavke.length; i++){
          if (this.sveStavke[i]["racunId"] == id){
            this.stavkeRacuna.push(this.sveStavke[i])
          }
        }

      }, err=> {console.log(err)})

  }}

}
