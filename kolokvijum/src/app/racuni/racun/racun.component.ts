import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { Racun } from 'src/app/model/racun';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-racun',
  templateUrl: './racun.component.html',
  styleUrls: ['./racun.component.css']
})
export class RacunComponent implements OnInit {
  public racun: Racun = {
    id: 0,
    datumRacuna: new Date()
  };
  public izmena: boolean = false;

  constructor(private httpClient:HttpClient, private router: Router, private aRoute: ActivatedRoute) { }

  ngOnInit(): void {
    console.log(this.izmena);
    
    this.izmena = (this.aRoute.snapshot.queryParams.izmeni =="true");
    console.log(this.izmena);
    this.httpClient.get("http://localhost:3000/racun/"+this.aRoute.snapshot.params.id).subscribe((racun:Racun)=>{
        this.racun = racun;
      }, err=> {console.log(err)})
  }

  submit(form: NgForm){
    this.httpClient.put("http://localhost:3000/racun/"+ this.racun.id, this.racun).subscribe((racun:Racun)=>{
      window.alert("Uspesno izmenjen racun");
      this.router.navigate(["/racuni"]);
    }, err=>{console.log(err)}
    )

  }
  nazad(){
    this.router.navigate(["/artikli"]);
  }

}
