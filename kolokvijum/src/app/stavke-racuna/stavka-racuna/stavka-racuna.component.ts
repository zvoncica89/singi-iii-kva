import { Component, OnInit } from '@angular/core';
import { StavkaRacuna } from 'src/app/model/stavkaRacuna';
import { HttpClient } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { Racun } from 'src/app/model/racun';
import { Artikal } from 'src/app/model/artikal';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-stavka-racuna',
  templateUrl: './stavka-racuna.component.html',
  styleUrls: ['./stavka-racuna.component.css']
})
export class StavkaRacunaComponent implements OnInit {
  public stavkaRacuna : StavkaRacuna = {
    id:null,
    racunId:null,
    artikalId:null,
    cena:null,
    kolicina:null
  }
  public racuni : Racun[] = [];
  public artikli: Artikal[] = [];
  public izmena: boolean = false;

  constructor(private httpClient:HttpClient, private router: Router, private aRoute: ActivatedRoute) { }

  getAllRacuni(){
    this.httpClient.get("http://localhost:3000/racun").subscribe((racuni:Racun[])=>{
        this.racuni = racuni;
      }, err=> {console.log(err)})

  }

  getAllArtikli(){
    this.httpClient.get("http://localhost:3000/artikal").subscribe((artikli:Artikal[])=>{
        this.artikli = artikli;
      }, err=> {console.log(err)})

  }

  ngOnInit(): void {
    this.getAllArtikli();
    this.getAllRacuni();
    this.izmena = (this.aRoute.snapshot.queryParams.izmeni =="true");
    this.httpClient.get("http://localhost:3000/stavkaRacuna/"+this.aRoute.snapshot.params.id).subscribe((stavkaRacuna:StavkaRacuna)=>{
        this.stavkaRacuna = stavkaRacuna;
      }, err=> {console.log(err)})
  }

  submit(form: NgForm){
    this.httpClient.put("http://localhost:3000/stavkaRacuna/"+ this.stavkaRacuna.id, this.stavkaRacuna).subscribe((stavkaRacuna:StavkaRacuna)=>{
      window.alert("Uspesno izmenjena stavka");
      this.router.navigate(["/stavke"]);
    }, err=>{console.log(err)}
    )

  }
  nazad(){
    this.router.navigate(["/stavke"]);
  }

}
