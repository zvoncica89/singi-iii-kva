import { Component, OnInit } from '@angular/core';
import { StavkaRacuna } from '../model/stavkaRacuna';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { NgForm } from '@angular/forms';
import { Racun } from '../model/racun';
import { Artikal } from '../model/artikal';

@Component({
  selector: 'app-stavke-racuna',
  templateUrl: './stavke-racuna.component.html',
  styleUrls: ['./stavke-racuna.component.css']
})
export class StavkeRacunaComponent implements OnInit {
  stavkeRacuna: StavkaRacuna[] = [];
  racuni:Racun[] = [];
  artikli:Artikal[] = [];
  dodaj: boolean = false;
  novaStavka: StavkaRacuna = {
    id:null,
    racunId:null,
    artikalId:null,
    cena:null,
    kolicina:null
  }
  constructor(private httpClient:HttpClient, private router: Router) { }

  gettAllStavke(){
    this.httpClient.get("http://localhost:3000/stavkaRacuna").subscribe((stavkeRacuna:StavkaRacuna[])=>{
        this.stavkeRacuna = stavkeRacuna;
      }, err=> {console.log(err)})
  }

  getAllRacuni(){
    this.httpClient.get("http://localhost:3000/racun").subscribe((racuni:Racun[])=>{
        this.racuni = racuni;
      }, err=> {console.log(err)})

  }

  getAllArtikli(){
    this.httpClient.get("http://localhost:3000/artikal").subscribe((artikli:Artikal[])=>{
        this.artikli = artikli;
      }, err=> {console.log(err)})

  }

  ngOnInit(): void {
    this.gettAllStavke(); 
    this.getAllRacuni();
    this.getAllArtikli();
  }

  ukloni(id){
    this.httpClient.delete("http://localhost:3000/stavkaRacuna/" + id).subscribe((stavka:StavkaRacuna)=>{
      window.alert("Obrisana stavka");
      this.gettAllStavke();
    }, err=>{console.log(err)})
  }

  detalji(id){
    this.router.navigate(['stavke', id], {queryParams: {izmeni : false}});
    
  }
  
  izmeni(id){
    this.router.navigate(['stavke', id], {queryParams: {izmeni : true}});
  }
  submit(form: NgForm){
    this.httpClient.post("http://localhost:3000/stavkaRacuna", this.novaStavka).subscribe((stavka:StavkaRacuna)=>{
        window.alert("Uspesno dodata nova stavka");
        this.gettAllStavke();
        this.dodaj = false;
      }, err=> {console.log(err)})
    
  }

}
