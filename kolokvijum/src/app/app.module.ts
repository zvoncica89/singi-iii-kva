import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ArtikliComponent } from './artikli/artikli.component';
import { RacuniComponent } from './racuni/racuni.component';
import { StavkeRacunaComponent } from './stavke-racuna/stavke-racuna.component';
import { RacunComponent } from './racuni/racun/racun.component';
import { ArtikalComponent } from './artikli/artikal/artikal.component';
import { StavkaRacunaComponent } from './stavke-racuna/stavka-racuna/stavka-racuna.component';

@NgModule({
  declarations: [
    AppComponent,
    ArtikliComponent,
    RacuniComponent,
    StavkeRacunaComponent,
    RacunComponent,
    ArtikalComponent,
    StavkaRacunaComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
